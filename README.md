# App plugin - VerifiedMail

Generates a verified e-mail address and stores it as a Verifiable credential in the Schluss app. It uses the [Schluss Identity Service](https://gitlab.com/schluss/schluss-identity-service) and [IOTA Identity](https://github.com/iotaledger/identity.rs/) to work its magic.
