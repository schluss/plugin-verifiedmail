# IOTA Identity Web connector

For now a 'web' connector is needed. It consists of a HTML page, loading the [IOTA Identity WASM library](https://github.com/iotaledger/identity.rs), compacted as a bundle.

Host the `/web` folder and specify the url in `/lib/model/mail_validation_model.dart` at `RequestUrl`.


## Compile the Web connector

Install the NPM packages
```
> npm install
```

Build the js bundle
```
> npm run build
```

Optionally: run a local dev server to host the HTML page
```
> npm run serve
```
