const path = require('path');
const CopyWebPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {

    mode: 'production',
    entry: './index.js',
    plugins: [
        new CopyWebPlugin({
            patterns: [{
                from: 'node_modules/@iota/identity-wasm/web/identity_wasm_bg.wasm',
                to: 'identity_wasm_bg.wasm'
            }],
        }),
    ],
    devServer: {
        contentBase: path.resolve(__dirname, './web'),
    },
    /*
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    compress: {
                        dead_code: false,
                        drop_console: true,
                        unused: false
                    },
                    mangle: {
                        reserved: ['runTest', 'reservedName2']
                    }
                }
            })
        ]
    },
    */
    output: {
        publicPath: '',
        path: path.resolve(__dirname, './web'),
        filename: 'bundle.js',
        library: {
            name: 'IOTA',
            type: 'window',
        },
    },
};
