import 'package:flutter/material.dart';
import 'package:verifiedmail/constants/ui_constants.dart';
import 'package:verifiedmail/ui/mail_validation_page.dart';
import 'package:verifiedmail/ui/reused_widgets/backward_button.dart';
import 'package:verifiedmail/ui/reused_widgets/closing_button.dart';
import 'package:verifiedmail/ui/reused_widgets/content_list.dart';
import 'package:verifiedmail/ui/reused_widgets/main_button.dart';
import 'package:verifiedmail/util/animated_route.dart';
import 'package:verifiedmail/util/translate.dart';

BuildContext? ctx;

class IntroPage extends StatelessWidget {
  final Locale locale;
  final Function callback;
  final String? pluginName;

  IntroPage(this.locale, this.callback,{
    this.pluginName});

  @override
  Widget build(BuildContext context) {
    ctx = context;

    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          height: height,
          width: width,
          child: Stack(
            children: [
              // content
              Container(
                color: UIConstants.primaryColor,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: height * 0.15,
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: 0,
                  ),
                  child: Column(
                    children: [
                      //SizedBox(height: 20),
                      Container(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        child: Image.asset(
                          'assets/images/email_verified.png',
                          package: 'verifiedmail',
                          width: width * 0.4,
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        Translate.txt('intro.title')!,
                        style: TextStyle(
                          color: UIConstants.headerTitleText,
                          fontSize: width * 0.05,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 20),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            Translate.txt('intro.description')!,
                            style: TextStyle(
                              color: UIConstants.mediumGrey,
                              fontSize: width * 0.05,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      ContentList([
                        ContentListItem('1', Translate.txt('intro.step1')),
                        ContentListItem('2', Translate.txt('intro.step2')),
                        ContentListItem('3', Translate.txt('intro.step3')),
                      ]),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),

              /*Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).padding.top + height * 0.1,
                ),
                height: height,
                width: width,
                color: UIConstants.primaryColor,
                child: 
              ),*/

              Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                Padding(
                  padding: EdgeInsets.only(
                    right: width * 0.075,
                    left: width * 0.075,
                    bottom: height * 0.05,
                  ),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: MainButton(
                        Translate.txt('intro.start'),
                        () async => {
                              await Navigator.push(
                                context,
                                AnimatedRoute(
                                  AnimationDirection.forward,
                                  builder: (context) => MailValidationPage(locale, callback,pluginName:pluginName),
                                ),
                              ),
                            }),
                  ),
                )
              ]),

              // header
              Stack(
                children: <Widget>[
                  Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: height * 0.1,
                        color: UIConstants.paleLilac,
                      )),
                  Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(20),
                            topRight: const Radius.circular(20),
                          ),
                        ),
                        height: height * 0.1,
                        alignment: Alignment.center,
                        child: Stack(
                          children: <Widget>[
                            // back arrow
                            Padding(
                              padding: EdgeInsets.only(left: height * 0.02),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: BackwardButton(
                                  locale: locale,
                                ),
                              ),
                            ),
                            // circle close button
                            Padding(
                              padding: EdgeInsets.only(right: height * 0.02),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: ClosingButton(),
                              ),
                            ),
                          ],
                        ),
                      )),
                ],
              ),
            ],
          ),
        ));
  }
}
