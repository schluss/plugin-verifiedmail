Map<String, String> NL = {
  // intro_page.dart
  'intro.title': 'Voeg je e-mail adres toe',
  'intro.description': 'Een gevalideerd en ondertekend e-mail adres in je kluis in 3 stappen:',
  'intro.step1': 'Voer je e-mail adres in',
  'intro.step2': 'Schluss stuurt je een bevestigingsmail',
  'intro.step3': 'Klik op de link in de mail',
  'intro.start': 'Start',

  // mail_validation_page.dart
  'validate.prepare': 'Voorbereiden...\n\nDit kan even duren',
  'validate.prepareFailed': 'Voorbereiden op het valideren van je e-mail adres is niet gelukt, mogelijk is de service tijdelijk onbeschikbaar. Probeer het later nogmaals',
  'validate.sendingMail': 'We sturen nu een e-mail aan ',
  'validate.sendingMailFailed': 'Het aanroepen van de Schluss validatie service is niet gelukt, mogelijk is deze tijdelijk onbeschikbaar. Probeer het later nogmaals',
  'validate.mailSent': 'Je hebt mail',
  'validate.whatIsYourMail': 'Wat is je e-mail adres?',
  'validate.validateYourMail': 'Valideer e-mail adres',
  'validate.genericErrorTitle': 'Oeps, er ging iets mis',
  'validate.startOver': 'Start opnieuw',
  'validate.clickMailLink': 'Klik op de link in de e-mail om verder te gaan.',
};

Map<String, String> EN = {
  // intro_page.dart
  'intro.title': 'Add your e-mail address',
  'intro.description': 'A validated and signed e-mail address in your vault in three steps:',
  'intro.step1': 'Enter your e-mail address',
  'intro.step2': 'Schluss sends an confirmation e-mail',
  'intro.step3': 'Click on the link inside the e-mail',
  'intro.start': 'Start',

  // mail_validation_page.dart
  'validate.prepare': 'Preparing...\n\nThis can take a while',
  'validate.prepareFailed': 'Preparing validation of your e-mail address failed. Possibly the service is temporary down for maintanance. Please try again later',
  'validate.sendingMail': 'We sturen nu een e-mail aan ',
  'validate.sendingMailFailed': 'Accessing the Schluss validation service failed. Possibly the service is temporary down for maintanance. Please try again later',
  'validate.mailSent': 'You\'ve got mail',
  'validate.whatIsYourMail': 'What is your e-mail address?',
  'validate.validateYourMail': 'Validate your e-mail address',
  'validate.genericErrorTitle': 'Oops, something went wrong',
  'validate.startOver': 'Start over',
  'validate.clickMailLink': 'Click on the link in the e-mail to continue.',
};

Map<String, Map> translations = {
  'NL': NL,
  'EN': EN,
  'UK': EN, // UK will be a copy of EN for now
};
