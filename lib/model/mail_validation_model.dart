import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';
import 'package:verifiedmail/verifiedmail.dart';

class MailValidationModel {
  // Fixed url, as long as we are not able to do a full WASM / FFI implementation of the IOTA RUST Identity library
  static const String RequestUrl = 'https://services.schluss.app/me/iota/index.html';

  Logger logger = Logger();
  HeadlessInAppWebView? headlessWebView;

  Future<Map?> getIdentity() async {
    var identity;

    // check if we have a stored identity
    var storedId = await PluginVerifiedMail.getFromSecureStore('iota.identity');

    // we don't have one yet, so create one on the fly
    if (storedId == '') {
      identity = await _createIdentity();

      // store the identity
      await _storeIdentity(identity);
    } else {
      identity = json.decode(storedId);
    }

    // share identity
    return identity;
  }

  Future<void> _storeIdentity(Map? identity) async {
    try {
      await PluginVerifiedMail.storeInSecureStore('iota.identity', json.encode(identity));
    } catch (e) {
      logger.e('could not store IOTA Identity in secure storage');
      rethrow;
    }
  }

  // create a new identity and publish it as a DID on IOTA
  Future<Map> _createIdentity() async {
    final completer = Completer<Map>();

    try {
      var idServiceApiUrl = PluginVerifiedMail.getEnvVar('IDENTITY_SERVICE_API_URL');
      var idServiceApiKey = PluginVerifiedMail.getEnvVar('IDENTITY_SERVICE_API_KEY');

      /* For specific debugging 
      if (Platform.isAndroid) {
        await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
      }
      */

      headlessWebView = HeadlessInAppWebView(
          initialUrlRequest: URLRequest(url: Uri.parse(RequestUrl)),
          initialOptions: InAppWebViewGroupOptions(crossPlatform: InAppWebViewOptions(clearCache: true)),
          onLoadStop: (controller, url) async {
            // Run javascript locally inside the loaded webview
            var functionBody = '''
              var p = new Promise(async function (resolve, reject){

                await window.identity.init();

                // Create a DID Document (an identity)
                const { doc, key } = new window.identity.Document(identity.KeyType.Ed25519);

                // Sign DID Document with key
                doc.sign(key);

                // return key and did
                resolve ({key : key.toJSON(), did : doc.toJSON()});

              });
              await p;
              return p;
        	  ''';

            var jsResult = await (controller.callAsyncJavaScript(functionBody: functionBody) as FutureOr<CallAsyncJavaScriptResult>);

            // Validate javascript result
            if (jsResult.value['key']['secret'] == null || jsResult.value['did'] == null || jsResult.error != null) {
              logger.e('identity service: createIdentity javascript function failed: ' + jsResult.error!);
              throw ('Generating keys and signed DID Document failed');
            }

            // Publish DID Document
            var httpClient = Dio();

            httpClient.options = BaseOptions(
              contentType: Headers.jsonContentType,
              headers: {
                'Authorization': idServiceApiKey,
              },
            );

            var response = await httpClient.post(idServiceApiUrl + '/identities', data: jsResult.value['did']);

            if (response.statusCode != 200) {
              logger.e('identity service: [POST] /identities failed, details:');
              logger.e(jsonEncode(response));

              throw ('Registering identity on IOTA ledger by publishing Signed DID document failed');
            }

            // Send back the result by completing the Completer
            completer.complete(jsResult.value);
          });

      // run the headless webview
      await headlessWebView!.run();

      return completer.future;
    } catch (e) {
      logger.e('identity service: createIdentity exception, details:');
      logger.e(e);
      rethrow;
    } finally {
      // remove the webview form memory
      dispose();
    }
  }

  void dispose() {
    headlessWebView ?? headlessWebView!.dispose();
  }

  // start a new email verification request
  Future<String?> startRequest(String? docId, String email) async {
    try {
      var idServiceApiUrl = PluginVerifiedMail.getEnvVar('IDENTITY_SERVICE_API_URL');
      var idServiceApiKey = PluginVerifiedMail.getEnvVar('IDENTITY_SERVICE_API_KEY');

      var httpClient = Dio();

      httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: {
          'Authorization': idServiceApiKey,
        },
      );

      var response = await httpClient.post(idServiceApiUrl + '/request', data: {
        'did': docId,
        'email': email,
      });

      if (response.statusCode != 200) {
        logger.e('plugin-verifiedmail: [POST] /request failed, details:');
        logger.e(jsonEncode(response));

        throw ('invalid response');
      }

      return response.data['requestId'];
    } catch (e) {
      logger.e('plugin-verifiedmail: startRequest exception, details:');
      logger.e(e);
      rethrow;
    }
  }

  // initiates polling for the status of the validation  request. When validated, return the details
  Future<Map<String, dynamic>?> awaitRequest(String? requestId) async {
    try {
      var httpClient = Dio();

      var idServiceApiUrl = PluginVerifiedMail.getEnvVar('IDENTITY_SERVICE_API_URL');
      var idServiceApiKey = PluginVerifiedMail.getEnvVar('IDENTITY_SERVICE_API_KEY');

      httpClient.options = BaseOptions(
        contentType: Headers.jsonContentType,
        headers: {
          'Authorization': idServiceApiKey,
        },
      );

      // try to get a valid result within 60 attempts (with a 3sec interval per attempt) = 3min in total
      for (var i = 0; i < 60; i++) {
        var response = await httpClient.get(idServiceApiUrl + '/request/' + requestId);

        // when there is an error
        if (response.statusCode == 401) {
          throw ('Invalid response from Schluss Identity Service while checking if e-mail address is verified');
        }

        // when there is a valid response
        if (response.statusCode == 200 || response.statusCode == 201) {
          // when verified by user
          if (response.data['verificationStatus'] == 'verified') {
            return {'EmailVc': jsonEncode(response.data['verifiableCredential'])};
          }
          // when verification failed
          else if (response.data['verificationStatus'] == 'failed') {
            throw ('Signature verificaton failed, therefore the e-mail address could not be verified');
          }
        }

        // still waiting, go for another round
        await Future.delayed(Duration(seconds: 3));
      }

      throw ('After a long time waiting with no sign of e-mail address being verified, the process is aborted. Please try again');
    } catch (e) {
      logger.e('plugin-verifiedmail: awaitRequest exception, details:');
      logger.e(e);

      return null;
    }
  }
}
