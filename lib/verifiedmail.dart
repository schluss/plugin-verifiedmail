library verifiedmail;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:verifiedmail/interface/common_plugin_dao.dart';
import 'package:verifiedmail/ui/intro_page.dart';
import 'package:verifiedmail/util/translate.dart';

Function? storeSecureFunc;
Function? getSecureFunc;

class PluginVerifiedMail implements CommonPluginDAO {
  static late Function getEnvVar;
  static late Function storeInSecureStore;
  static late Function getFromSecureStore;

  final StreamController<int> _streamController = StreamController.broadcast();

  // run plugin
  @override
  void runPlugin(
    context,
    var jsonProvider,
    var jsonProviderData,
    var dataModel,
    Function callBack,
    {
      String pluginName = 'VERIFIED MAIL',
    }
  ) async {
    var locale = Localizations.localeOf(context); // Sets localization.

    // Set the translation locale for the whole plugin
    Translate.setLocale(locale);

/*
    storeSecureFunc = storeSecure;
    getSecureFunc = getSecure;

    await storeSecure('test', 'Dit is een test');

    String ret = await getSecure('test');

    String asd = ret;

    // Store the 'get identity' method for later use
    //MailValidationModel.getIdentity = getIdentity;
*/
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => IntroPage(locale, callBack,pluginName: pluginName,),
      ),
    );
  }

  /// Extracts data and sets into a map of attributes.
  @override
  Future<Map<String, dynamic>> getExtractedData(String pluginData) async {
    // set progress to 100%
    _streamController.add(100);

    return Future.value(jsonDecode(pluginData));
  }

  /// Gets the progress value of the process.
  Stream<int> getProgress() {
    return _streamController.stream; // Returns stream controller to the main app.
  }
}
