import 'dart:ui';

import 'package:verifiedmail/constants/translations.dart';

// Handle translations within the plugin
class Translate {
  static String? language;

  // Set the locale: like NL, EN
  static void setLocale(Locale locale) {
    language = locale.languageCode.toUpperCase().toString();
  }

  // Translate given translation replacement string
  static String? txt(String translate) {
    if (!translations.containsKey(language)) {
      return translate;
    }

    var lang = translations[language!]!;

    if (!lang.containsKey(translate)) {
      return translate;
    }

    return lang[translate];
  }
}
