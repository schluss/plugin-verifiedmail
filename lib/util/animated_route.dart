import 'package:flutter/material.dart';

class AnimationDirection {
  static const Offset forward = Offset(1, 0); // right to left animation
  static const Offset backward = Offset(-1, 0); // left to right animation
  static const Offset upward = Offset(0, 1); // bottom to up navigation
  static const Offset downward = Offset(0, -1); // top to bottom animation
}

class AnimatedRoute<T> extends MaterialPageRoute<T> {
  Offset direction = AnimationDirection.forward;

  AnimatedRoute(this.direction, {required WidgetBuilder builder, RouteSettings? settings}) : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: AnimationDirection.forward,
        end: Offset.zero,
      ).animate(CurvedAnimation(
        parent: animation,
        curve: Curves.fastOutSlowIn,
      )),
      child: child,
    );
  }
}
